from pyfiglet import Figlet
f = Figlet()
s = Figlet(font='digital')
b = Figlet(font="banner3-D")

def prRed(prt): return("\033[91m{}\033[00m" .format(prt))
def prGreen(prt): return("\033[92m{}\033[00m" .format(prt))
def prYellow(prt): return("\033[93m{}\033[00m" .format(prt))
def prLightPurple(prt): return("\033[94m{}\033[00m" .format(prt))
def prPurple(prt): return("\033[95m{}\033[00m" .format(prt))
def prCyan(prt): return("\033[96m{}\033[00m" .format(prt))
def prBold(prt): return("\033[1m{}\033[00m" .format(prt))
def prUnderline(prt): return("\033[4m{}\033[00m" .format(prt))

import mysql.connector as mysql

#Welcome Screen
def Welcome():
    print("\n")
    print(prCyan(b.renderText('WELCOME')))
    print(prGreen(f.renderText('NYC 311 CLIENT')))
    #print("WELCOME TO THE NYC 311 CLIENT.....\n")
    print(prBold(prRed(s.renderText("PLEASE SELECT THE OPERATING USER"))))
    print(prPurple("1. Non Registered User"))
    print(prPurple("2. Registered User"))
    print(prPurple("3. Agency User"))
    print(prPurple("4. Administrator\n"))
    user=int(input(prYellow("ENTER YOUR CHOICE: ")))
    return user

#Login data functon
def Login(userType):
    if userType==1:
        user = "NonRegisteredUser"
        password = "1854"
        NonRegisteredUser(user,password)
    elif userType==2:
        user = "RegisteredUser"
        password = "1854"
        RegisteredUser(user,password)
    elif userType==3:
        user = "AgencyUser"
        password = "1854"
        AgencyUser(user,password)
    elif userType==4:
        user = "Administrator"
        password = "1854"
        AdministratorUser(user,password)
    else:
        print("WRONG INPUT. PLEASE TRY AGAIN.......")

#Setup Function
def Setup(user,password):
    #Variables
    host = "localhost"
    #Connecting to mysql
    try:
        db = mysql.connect(host=host,user=user,password=password)
        print("Connected successfully")
    except Exception as e:
        print(e)
        print("Failed to connect")

    #Creating a cursor
    try:
        command_handler = db.cursor()
        #print("Created cursor successfully")
    except Exception as e:
        print("Could not create cursor")

    #Connecting to an existing database
    try:
        db1 = mysql.connect(host=host,user=user,password=password,database="Project")
        #print("Connected to Project database")
    except Exception as e:
        print("Could not connect to project database")
        print(e)

    #Creating a command handler
    try:
        command_handler = db1.cursor()
        #print("Created command handler successfully")
    except Exception as e:
        print("Cannot create command handler")
        print(e)
    return db1,command_handler

#Function to take Year and City input
def YearCityInput():
    year=input(prYellow("Enter the Year: "))
    city=input(prYellow("Enter the City Name: "))
    print("\n")
    return year,city

#Function to the count from database
def countsFunction(command,command_handler):
    command_handler.execute(command)
    records = command_handler.fetchall()
    ans=-1
    for record in records:
        ans=record[0]
    return str(ans)

#Function to return count of complaints handled by each agency
def AgencyWiseComplaints(command,command_handler):
    command_handler.execute(command)
    records = command_handler.fetchall()
    for record in records:
        print(prPurple("Number of complaints handled by "+record[0]+" Agency are "+str(record[1])))

#Function to submit a request
def SubmitRequest(db1,command_handler):
    print(prUnderline(prBold(prGreen("Select the type of Request"))))
    print(prPurple("1. Taxi Request"))
    print(prPurple("2. Bridge/Highway Incident"))
    print(prPurple("3. Other request/complaint"))
    inp=int(input(prYellow("\nEnter your choice:")))
    ComplaintType=input(prCyan("\nComplaint Type: "))
    Descriptor=input(prCyan("Enter Description: "))
    Agency=input(prCyan("Enter Agency: "))
    AgencyName=input(prCyan("Enter Agency Name: "))
    LocationType=input(prCyan("Enter Location Type: "))
    AddressType=input(prCyan("Enter Address Type: "))
    StreetName=input(prCyan("Enter Street Name: "))
    IncidentAddress=input(prCyan("Enter Incident Address: "))
    Landmark=input(prCyan("Enter Landmark: "))
    IncidentZip=input(prCyan("Enter Incident ZipCode: "))
    City=input(prCyan("Enter City Name: "))
    AgencyTypeEntry = "INSERT IGNORE INTO AgencyType(Agency) VALUES ('"+Agency+"');"
    command_handler.execute(AgencyTypeEntry)
    db1.commit()
    command_handler.execute("SELECT AgencyTypeID FROM AgencyType where Agency='"+Agency+"';")
    records = command_handler.fetchall()
    AgencyTypeID=-1
    for record in records:
        AgencyTypeID=record[0]
    #print(AgencyTypeID)
    AgencyNameEntry = "INSERT IGNORE INTO AgencyData(AgencyTypeID,AgencyName) VALUES ("+str(AgencyTypeID)+",'"+AgencyName+"');"
    command_handler.execute(AgencyNameEntry)
    db1.commit()
    command_handler.execute("SELECT AgencyDataID FROM AgencyData where AgencyName='"+AgencyName+"';")
    records = command_handler.fetchall()
    AgencyDataID=-1
    for record in records:
        AgencyDataID=record[0]
    #print(AgencyDataID)
    CityDataEntry = "INSERT IGNORE INTO CityData(IncidentZip,City) VALUES ("+str(IncidentZip)+",'"+City+"');"
    command_handler.execute(CityDataEntry)
    db1.commit()
    UniqueKey=100000000
    command_handler.execute("SELECT max(UniqueKey) FROM RequestData;")
    records = command_handler.fetchall()
    for record in records:
        UniqueKey=record[0]
    UniqueKey=UniqueKey+1
    if inp==1:
        Borough=input(prCyan("Enter Borough: "))
        VehicleType=input(prCyan("Enter Vehicle Type: "))
        TaxiPickUpLocation=input(prCyan("Enter Taxi Pick Up Location: "))
        TaxiCompanyBorough=input(prCyan("Enter Taxi Company Borough: "))
        TaxiIncidentDataEntry="INSERT IGNORE INTO TaxiIncidentData(VehicleType,TaxiPickUpLocation,TaxiCompanyBorough) VALUES('"+VehicleType+"','"+TaxiPickUpLocation+"','"+TaxiCompanyBorough+"');"
        #print(TaxiIncidentDataEntry)
        command_handler.execute(TaxiIncidentDataEntry)
        db1.commit()
        #print("Completed")
        command_handler.execute("SELECT TaxiIncidentID FROM TaxiIncidentData where VehicleType='"+VehicleType+"' AND TaxiPickUpLocation='"+TaxiPickUpLocation+"' AND TaxiCompanyBorough='"+TaxiCompanyBorough+"';")
        records = command_handler.fetchall()
        TaxiIncidentID=-1
        for record in records:
            TaxiIncidentID=record[0]
        ComplaintsWithBoroughDataEntry="INSERT IGNORE INTO ComplaintsWithBorough(Borough,TaxiIncidentID) VALUES('"+Borough+"',"+str(TaxiIncidentID)+");"
        #print(ComplaintsWithBoroughDataEntry)
        command_handler.execute(ComplaintsWithBoroughDataEntry)
        db1.commit()
        command_handler.execute("SELECT BoroughID FROM ComplaintsWithBorough where Borough='"+Borough+"' AND TaxiIncidentID="+str(TaxiIncidentID)+";")
        records = command_handler.fetchall()
        BoroughID=-1
        for record in records:
            BoroughID=record[0]
        RequestDataEntry = "INSERT IGNORE INTO RequestData(Status,UniqueKey,AgencyDataID,BoroughID,CreatedDate,ComplaintType,Descriptor,LocationType,AddressType,StreetName,IncidentAddress,IncidentZip,Landmark) VALUES ('OPEN',"+str(UniqueKey)+","+str(AgencyDataID)+","+str(BoroughID)+",CURDATE(),'"+ComplaintType+"','"+Descriptor+"','"+LocationType+"','"+AddressType+"','"+StreetName+"','"+IncidentAddress+"',"+str(IncidentZip)+",'"+Landmark+"');"
    elif inp==2:
        Borough=input(prCyan("Enter Borough: "))
        BridgeHighwayName=input(prCyan("Enter Bridge/Highway Name: "))
        BridgeHighwayDirection=input(prCyan("Enter Bridge/Highway Direction: "))
        BridgeHighwaySegment=input(prCyan("Enter Bridge/Highway Segment: "))
        RoadRamp=input(prCyan("Enter RoadRamp: "))
        BridgeHighwayIncidentDataEntry="INSERT IGNORE INTO BridgeHighwayIncidentData(BridgeHighwayName,BridgeHighwayDirection,BridgeHighwaySegment,RoadRamp) VALUES('"+BridgeHighwayName+"','"+BridgeHighwayDirection+"','"+BridgeHighwaySegment+"','"+RoadRamp+"');"
        #print(BridgeHighwayIncidentDataEntry)
        command_handler.execute(BridgeHighwayIncidentDataEntry)
        db1.commit()
        #print("Completed")
        command_handler.execute("SELECT BridgeHighwayIncidentID FROM BridgeHighwayIncidentData where BridgeHighwayName='"+BridgeHighwayName+"' AND BridgeHighwayDirection='"+BridgeHighwayDirection+"' AND BridgeHighwaySegment='"+BridgeHighwaySegment+"' AND RoadRamp='"+RoadRamp+"';")
        records = command_handler.fetchall()
        BridgeHighwayIncidentID=-1
        for record in records:
            BridgeHighwayIncidentID=record[0]
        ComplaintsWithBoroughDataEntry="INSERT IGNORE INTO ComplaintsWithBorough(Borough,BridgeHighwayIncidentID) VALUES('"+Borough+"',"+str(BridgeHighwayIncidentID)+");"
        #print(ComplaintsWithBoroughDataEntry)
        command_handler.execute(ComplaintsWithBoroughDataEntry)
        db1.commit()
        command_handler.execute("SELECT BoroughID FROM ComplaintsWithBorough where Borough='"+Borough+"' AND BridgeHighwayIncidentID="+str(BridgeHighwayIncidentID)+";")
        records = command_handler.fetchall()
        BoroughID=-1
        for record in records:
            BoroughID=record[0]
        RequestDataEntry = "INSERT IGNORE INTO RequestData(Status,UniqueKey,AgencyDataID,BoroughID,CreatedDate,ComplaintType,Descriptor,LocationType,AddressType,StreetName,IncidentAddress,IncidentZip,Landmark) VALUES ('OPEN',"+str(UniqueKey)+","+str(AgencyDataID)+","+str(BoroughID)+",CURDATE(),'"+ComplaintType+"','"+Descriptor+"','"+LocationType+"','"+AddressType+"','"+StreetName+"','"+IncidentAddress+"',"+str(IncidentZip)+",'"+Landmark+"');"

    elif inp==3:
        RequestDataEntry = "INSERT IGNORE INTO RequestData(Status,UniqueKey,AgencyDataID,CreatedDate,ComplaintType,Descriptor,LocationType,AddressType,StreetName,IncidentAddress,IncidentZip,Landmark) VALUES ('OPEN',"+str(UniqueKey)+","+str(AgencyDataID)+",CURDATE(),'"+ComplaintType+"','"+Descriptor+"','"+LocationType+"','"+AddressType+"','"+StreetName+"','"+IncidentAddress+"',"+str(IncidentZip)+",'"+Landmark+"');"
        #print(RequestDataEntry)
    command_handler.execute(RequestDataEntry)
    db1.commit()
    print(prBold(prPurple("\nYOUR REQUEST IS SUCCESSFULLY SUBMITTED AND YOUR REQUEST ID IS: "+str(UniqueKey)+". PLEASE SAVE IT FOR YOUR FUTURE REFERENCE.")))

#Function to the count from database
def PrintMultipleRecordsFunction(command,command_handler):
    command_handler.execute(command)
    records = command_handler.fetchall()
    for record in records:
        print(prPurple(record[0]))

#Function to the print data related to unique key from database
def UpdateRequestUniqueKeyData(command,command_handler):
    command_handler.execute(command)
    records = command_handler.fetchall()
    for record in records:
        print(prPurple("Created Date: "+str(record[2])))
        print(prPurple("Agency :"+record[1]))
        print(prPurple("Status: "+str(record[6])))
        print(prPurple("Complaint Type: "+record[4]))
        print(prPurple("Descriptor: "+record[5]))
        print(prPurple("City: "+record[3]))
        print(prPurple("Address: "+record[7]))
        print(prPurple("Landmark: "+record[8]))
        print(prPurple("ResolutionActionUpdateDate "+str(record[9])))
        print(prPurple("ResolutionDescription: "+str(record[10])))
        print(prPurple("Closed Date: "+str(record[11])))

#Function to the print data related to unique key from database
def UniqueKeyData(command,command_handler):
    command_handler.execute(command)
    records = command_handler.fetchall()
    for record in records:
        print(prPurple("Created Date: "+str(record[0])))
        print(prPurple("Agency :"+record[10]))
        print(prPurple("Complaint Type: "+record[1]))
        print(prPurple("Descriptor: "+record[2]))
        print(prPurple("Address Type: "+record[7]))
        print(prPurple("Location Type: "+record[3]))
        print(prPurple("Street Name: "+record[6]))
        print(prPurple("Address: "+record[5]))
        print(prPurple("Landmark: "+record[8]))
        print(prPurple("City: "+record[9]))
        print(prPurple("ZipCode: "+str(record[4])))
        print(prPurple("Status: "+str(record[11])))
        print(prPurple("ResolutionDescription: "+str(record[12])))
        print(prPurple("Closed Date: "+str(record[13])))

#Exit function
def ExitFunction():
    print("\n")
    print(prCyan(b.renderText('BYE BYE')))
    #print("EXITING THE APPLICATION..................BYE BYE")
    exit(0)

#Non Registered User Code
def NonRegisteredUser(user,password):
    db1,command_handler=Setup(user,password)
    print("\n")
    print(prCyan(s.renderText('WELCOME NON REGISTERED USER')))
    year,city=YearCityInput()
    while(True):
        print(prUnderline(prBold(prGreen("PLEASE SELECT THE TASK TO BE PERFORMED"))))
        print(prPurple("1. Number of requests handled by each agency in "+city+" in the year "+str(year)))
        print(prPurple("2. Total number of requests in "+city+" in the year "+str(year)))
        print(prPurple("3. Total number of Taxi requests in "+city+" in the year "+str(year)))
        print(prPurple("4. Total number of Bridge/Highway requests in in "+city+" in the year "+str(year)))
        print(prPurple("5. To change city and year details"))
        print(prPurple("Any other number to exit\n"))
        userInput=int(input(prYellow("Enter your input: ")))
        if userInput==1:
            print(prBold(prRed("\nYOU HAVE SELECTED: "+"'"+"Number of requests handled by each agency in "+city+" in the year "+str(year)+"'")))
            command="select Agency,count(Agency) from NRUV where City='"+city+"' AND CreatedYear="+year+" GROUP BY Agency;"
            print(prCyan("Loading...."))
            AgencyWiseComplaints(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==2:
            print(prBold(prRed("\nYOU HAVE SELECTED: "+"'"+"Total number of requests in "+city+" in the year "+str(year)+"'")))
            command="select count(*) from NRUV where City='"+city+"' AND CreatedYear="+year+";"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple("Total number of requests in "+str(year)+" in the city "+city+" : "+ans))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==3:
            print(prBold(prRed("\nYOU HAVE SELECTED: "+"'"+"Total number of Taxi requests in "+city+" in the year "+str(year)+"'")))
            command="select count(*) from NRUV where City='"+city+"'AND CreatedYear="+year+" and TaxiIncidentID IS NOT NULL;"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple("Total number of Taxi requests in "+str(year)+" in the city "+city+" : "+ans))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==4:
            print(prBold(prRed("\nYOU HAVE SELECTED: "+"'"+"Total number of Bridge/Highway requests in in "+city+" in the year "+str(year)+"'")))
            command="select count(*) from NRUV where City='"+city+"' AND CreatedYear="+year+" and BridgeHighwayIncidentID IS NOT NULL;"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple("Total number of Bridge/Highway requests in "+str(year)+" in the city "+city+" : "+ans))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==5:
            print(prBold(prGreen("\nENTER THE NEW INPUTS")))
            year,city=YearCityInput()
        else:
            ExitFunction()
            
#Registered User Code
def RegisteredUser(user,password):
    db1,command_handler=Setup(user,password)
    print("\n")
    print(prBold(prCyan(s.renderText('WELCOME REGISTERED USER'))))
    while(True):
        print(prUnderline(prBold(prGreen("PLEASE SELECT THE TASK TO BE PERFORMED"))))
        print(prPurple("1. SUBMIT A REQUEST"))
        print(prPurple("2. Total number of requests received and closed by an agency in the given year"))
        print(prPurple("3. Agency that recieved maximum complaints in a given year "))
        print(prPurple("4. City that registered maximum complaints in a given year "))
        print(prPurple("5. City that registered maximum complaints with a given agency "))
        print(prPurple("6. Agency with which maximum complaints were registered in a given city"))
        print(prPurple("7. City from which Highest Taxi Requests were recieved"))
        print(prPurple("8. City from which Highest Bridge/Highway Requests were recieved"))
        print(prPurple("Any other number to exit\n"))
        userInput=int(input(prYellow("Enter your input: ")))
        if userInput==1:
            print(prBold(prRed("YOU HAVE SELECTED: 'SUBMIT A REQUEST'\n")))
            SubmitRequest(db1,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==2:
            print(prBold(prRed("YOU HAVE SELECTED: 'Total number of requests received and closed by an agency in the given year'\n")))
            year=input(prYellow("Enter the Year: "))
            agency=input(prYellow("Enter the Agency Name: "))
            command1="select count(Agency) from RUV where Agency='"+agency+"' AND CreatedYear="+year+" GROUP BY Agency;"
            command2="select count(Agency) from RUV where Agency='"+agency+"' AND CreatedYear="+year+" AND ClosedDate IS NOT NULL GROUP BY Agency;"
            print(prCyan("Loading...."))
            ans1=countsFunction(command1,command_handler)
            ans2=countsFunction(command2,command_handler)
            print(prPurple("Total number of requests recieved by "+agency+" in the year "+str(year)+" : "+ans1))
            print(prPurple("Total number of requests closed by "+agency+" in the year "+str(year)+" : "+ans2))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==3:
            print(prBold(prRed("YOU HAVE SELECTED: 'Agency that recieved maximum complaints in a given year'\n")))
            year=input(prYellow("Enter the Year: "))
            command="WITH T1 as (SELECT Agency,count(Agency) as AgencyCounts from RUV WHERE CreatedYear="+str(year)+" GROUP BY Agency) SELECT Agency from T1 where AgencyCounts=(Select max(AgencyCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" Agency recieved maximum complaints in the year "+str(year)))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            print("\n")
            if val==0:
                ExitFunction()
        elif userInput==4:
            print(prBold(prRed("YOU HAVE SELECTED: 'City that registered maximum complaints in a given year'\n")))
            year=input(prYellow("Enter the Year: "))
            command="WITH T1 as (SELECT City,count(City) as CityCounts from RUV WHERE CreatedYear="+str(year)+" GROUP BY City) SELECT City from T1 where CityCounts=(Select max(CityCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" city have registered maximum complaints in the year "+str(year)))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==5:
            print(prBold(prRed("YOU HAVE SELECTED: 'City that registered maximum complaints with a given agency '\n")))
            agency=input(prYellow("Enter Agency Name: "))
            command="WITH T1 as (SELECT City,count(Agency) as AgencyCounts from RUV WHERE Agency='"+agency+"' GROUP BY City) SELECT City from T1 where AgencyCounts=(Select max(AgencyCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" city have registered maximum complaints with the Agency "+agency))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==6:
            print(prBold(prRed("YOU HAVE SELECTED: 'Agency with which maximum complaints were registered in a given city'\n")))
            city=input(prYellow("Enter the City name: "))
            command="WITH T1 as (SELECT Agency,count(Agency) as AgencyCounts from RUV WHERE City='"+city+"' GROUP BY Agency) SELECT Agency from T1 where AgencyCounts=(Select max(AgencyCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" have recieved maximum requests from the city "+city))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==7:
            print(prBold(prRed("YOU HAVE SELECTED: 'City from which Highest Taxi Requests were recieved'\n")))
            command="WITH T1 as (SELECT City,count(TaxiIncidentID) as TaxiCounts from RUV WHERE TaxiIncidentID IS NOT NULL and City!='N/A' GROUP BY City) SELECT City from T1 where TaxiCounts=(Select max(TaxiCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" have requested maximum taxi requests"))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==8:
            print(prBold(prRed("YOU HAVE SELECTED: 'City from which Highest Bridge/Highway Requests were recieved'\n")))
            command="WITH T1 as (SELECT City,count(BridgeHighwayIncidentID) as BridgeHighwayCounts from RUV WHERE BridgeHighwayIncidentID IS NOT NULL and City!='N/A' GROUP BY City) SELECT City from T1 where BridgeHighwayCounts=(Select max(BridgeHighwayCounts) from T1);"
            print(prCyan("Loading...."))
            ans=countsFunction(command,command_handler)
            print(prPurple(ans+" have requested maximum bridge/highway requests"))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        else:
            ExitFunction()

#Agency User Function
def AgencyUser(user,password):
    db1,command_handler=Setup(user,password)
    print("\n")
    print(prBold(prCyan(s.renderText('WELCOME AGENCY USER'))))
    while(True):
        print(prUnderline(prBold(prGreen("PLEASE SELECT THE TASK TO BE PERFORMED"))))
        print(prPurple("1. UNIQUE KEYS OF REQUESTS SUBMITTED TODAY BASED ON AGENCY NAME"))
        print(prPurple("2. REQUEST DETAILS BASED ON UNIQUE KEY"))
        print(prPurple("3. UPDATE A REQUEST"))
        print(prPurple("Any other number to exit\n"))
        userInput=int(input(prYellow("Enter your input: ")))
        if userInput==1:
            print(prBold(prRed("YOU HAVE SELECTED: 'UNIQUE KEYS OF REQUESTS SUBMITTED TODAY BASED ON AGENCY NAME'\n")))
            AgencyName=input(prYellow("Enter Agency Name:"))
            command="SELECT UniqueKey from AUV where CreatedDate=CURDATE() AND Agency='"+AgencyName+"';"
            print(prCyan("Loading...."))
            print(prUnderline(prBold(prGreen("The Unique Keys of requests created today are:"))))
            PrintMultipleRecordsFunction(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==2:
            print(prBold(prRed("YOU HAVE SELECTED: 'REQUEST DETAILS BASED ON UNIQUE KEY'\n")))
            UniqueKey=int(input(prYellow("Enter Request Unique Key: ")))
            command="SELECT * from AUV where UniqueKey="+str(UniqueKey)+";"
            print(prCyan("Loading...."))
            print(prUnderline(prGreen("REQUEST DATA OF "+str(UniqueKey))))
            UpdateRequestUniqueKeyData(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==3:
            print(prBold(prRed("YOU HAVE SELECTED: 'UPDATE A REQUEST'\n")))
            UniqueKey=int(input(prYellow("Enter Request Unique Key: ")))
            ResolutionDescription=input(prYellow("Enter Resolution Description: "))
            command="UPDATE AUV SET ResolutionDesciption='"+ResolutionDescription+"',ResolutionActionUpdateDate=CURDATE() WHERE UniqueKey="+str(UniqueKey)+";"
            print(prCyan("Loading...."))
            command_handler.execute(command)
            print(prPurple("RECORD UPDATED"))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        else:
            ExitFunction()

# Administrator User Function
def AdministratorUser(user,password):
    db1,command_handler=Setup(user,password)
    print("\n")
    print(prBold(prCyan(s.renderText('WELCOME ADMINISTRATOR USER'))))
    while(True):
        print(prUnderline(prBold(prGreen("PLEASE SELECT THE TASK TO BE PERFORMED"))))
        print(prPurple("1. UNIQUE KEYS OF ALL REQUESTS SUBMITTED TODAY"))
        print(prPurple("2. REQUEST DETAILS BASED ON UNIQUE KEY"))
        print(prPurple("3. UPDATE REQUEST DETAILS BASED ON UNIQUE KEY"))
        print(prPurple("4. DELETE REQUEST DETAILS BASED ON UNIQUE KEY"))
        print(prPurple("5. DISPLAY TABLE NAMES IN THE DATABASE"))
        print(prPurple("6. ADD A NEW TABLE"))
        print(prPurple("7. MODIFY A TABLE SCHEMA"))
        print(prPurple("8. DROP A TABLE"))
        print(prPurple("Any other number to exit\n"))
        userInput=int(input(prYellow("Enter your input: ")))
        if userInput==1:
            print(prBold(prRed("YOU HAVE SELECTED: 'UNIQUE KEYS OF REQUESTS SUBMITTED TODAY'\n")))
            command="SELECT UniqueKey from RequestData where CreatedDate=CURDATE();"
            print(prCyan("Loading...."))
            print(prUnderline(prGreen("The Unique Keys of requests created today are: ")))
            PrintMultipleRecordsFunction(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==2:
            print(prBold(prRed("YOU HAVE SELECTED: 'REQUEST DETAILS BASED ON UNIQUE KEY'\n")))
            UniqueKey=int(input(prYellow("Enter Request Unique Key: ")))
            command="select CreatedDate,ComplaintType,Descriptor,LocationType,CityData.IncidentZip,IncidentAddress,StreetName,AddressType,Landmark,City,Agency,Status,ResolutionDesciption,ClosedDate from RequestData inner join CityData inner join AgencyData inner join AgencyType where RequestData.IncidentZip=CityData.IncidentZip and RequestData.AgencyDataID=AgencyData.AgencyDataID and AgencyData.AgencyTypeID=AgencyType.AgencyTypeID and UniqueKey="+str(UniqueKey)+";"
            print(prCyan("Loading...."))
            print(prUnderline(prGreen("REQUEST DATA OF "+str(UniqueKey))))
            UniqueKeyData(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==3:
            print(prBold(prRed("YOU HAVE SELECTED: 'UPDATE REQUEST DETAILS BASED ON UNIQUE KEY'\n")))
            UniqueKey=int(input(prYellow("Enter Request Unique Key: ")))
            Status=input("Enter Status Value: ")
            command="UPDATE RequestData SET Status='"+Status+"',ClosedDate=CURDate() where UniqueKey="+str(UniqueKey)+";"
            command_handler.execute(command)
            print("REQUEST UPDATED")
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==4:
            print(prBold(prRed("YOU HAVE SELECTED: 'DELETE REQUEST DETAILS BASED ON UNIQUE KEY'\n")))
            UniqueKey=int(input(prYellow("Enter Request Unique Key: ")))
            command="DELETE FROM RequestData WHERE UniqueKey="+str(UniqueKey)+";"
            print(prCyan("Loading...."))
            command_handler.execute(command)
            print("REQUEST DELETED")
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==5:
            print(prBold(prRed("YOU HAVE SELECTED: 'DISPLAY TABLE NAMES IN THE DATABASE'\n")))
            command="SHOW TABLES;"
            print(prUnderline(prGreen("TABLE NAMES:")))
            PrintMultipleRecordsFunction(command,command_handler)
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==6:
            print(prBold(prRed("YOU HAVE SELECTED: 'ADD A NEW TABLE'\n")))
            tableName=input(prYellow("Enter Table Name:"))
            columns=int(input(prYellow("Enter Number of Attributes: ")))
            columnsNames=[]
            dataTypes=[]
            for i in range(columns):
                attributeName=input(prCyan("Enter Attribute Name: "))
                dataType=input(prLightPurple("Enter Attribute DataType: "))
                columnsNames.append(attributeName)
                dataTypes.append(dataType)
            command="CREATE TABLE "+tableName+"("
            for i in range(columns-1):
                command=command+columnsNames[i]+" "
                command=command+dataTypes[i]+","
            command=command+columnsNames[-1]+" "
            command=command+dataTypes[-1]+");"
            command_handler.execute(command)
            print(prPurple(tableName+" Created SUCCESSFULLY"))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==7:
            print(prBold(prRed("YOU HAVE SELECTED: 'MODIFY A TABLE SCHEMA'\n")))
            tableName=input(prYellow("Enter Table Name:"))
            print(prGreen(prUnderline("SELECT AN OPTION:")))
            print(prPurple("1. ADD an atrribute"))
            print(prPurple("2. DROP an attribute"))
            choice=int(input(prYellow("Enter your choice:")))
            if choice==1:
                attributeName=input(prCyan("Enter Attribute Name: "))
                dataType=input(prLightPurple("Enter Attribute DataType: "))
                command="ALTER TABLE "+tableName+" ADD "+attributeName+" "+dataType+";"
                command_handler.execute(command)
                print(prPurple("Attribute "+attributeName+" Added Successfully to the table "+tableName))
            elif choice==2:
                attributeName=input(prRed("Enter Attribute Name: "))
                command="ALTER TABLE "+tableName+" DROP COLUMN "+attributeName+";"
                command_handler.execute(command)
                print(prPurple("Attribute "+attributeName+" dropped Successfully to the table "+tableName))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        elif userInput==8:
            print(prBold(prRed("YOU HAVE SELECTED: 'DROP A TABLE'\n")))
            tableName=input(prRed("Enter Table Name:"))
            command="DROP TABLE "+tableName+";"
            command_handler.execute(command)
            print(prPurple(tableName+" table dropped Successfully from the database"))
            val=int(input(prYellow("\nPress 1 to continue and 0 to exit : ")))
            if val==0:
                ExitFunction()
        else:
            ExitFunction()

#Main Function
if __name__ == "__main__":
    userType=Welcome()
    Login(userType)