# ECE 656 - Group 3 - Fall 2021
### Step-1: RUN server.sql to create and load database, command- source server.sql
### Step-2: RUN Client.py to access the database, command: python3 Client.py
### Step-3: Run TestCases.py to run testcases, Command: python3 -m unittest TestCases.py
