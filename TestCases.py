import unittest
import mysql.connector as mysql

#Function for database user login
def database_Login(user,password):
    try:
        db = mysql.connect(host="localhost",user=user,password=password)
        return "Connected successfully"
    except Exception as e:
        return "Failed to connect"

#Command Parser Function
def command_Parser(host,user,password,command):
    try:
        db1 = mysql.connect(host=host,user=user,password=password,database="Project")
    except Exception as e:
        print("Could not connect to project database")
    try:
        command_handler = db1.cursor()
    except Exception as e:
        print("Cannot create command handler")
    try:
        command_handler.execute(command)
        return "Executed"
    except Exception as e:
        return "Cannot Execute"

#Non Registered User Command Execution Function
def Non_Registered_User_Permission(command):
    host="localhost"
    user="NonRegisteredUser"
    password="1854"
    return command_Parser(host,user,password,command)

#Registered User Command Execution Function
def Registered_User_Permission(command):
    host="localhost"
    user="RegisteredUser"
    password="1854"
    return command_Parser(host,user,password,command)

#Agency User Command Execution Function
def Agency_User_Permission(command):
    host="localhost"
    user="AgencyUser"
    password="1854"
    return command_Parser(host,user,password,command)

#Administrator Command Execution Function
def Administrator_Permission(command):
    host="localhost"
    user="Administrator"
    password="1854"
    return command_Parser(host,user,password,command)

#Testing Class
class ProjectTesting(unittest.TestCase):
    #Non Registered User Login Testing
    def test_NonRegisteredUser_database_Login(self):
        self.assertEqual(database_Login("NonRegisteredUser","185"), "Failed to connect")
        self.assertEqual(database_Login("NonRegisteredUser","1854"), "Connected successfully")
    
    #Registered User Login Testing
    def test_RegisteredUser_database_Login(self):
        self.assertEqual(database_Login("RegisteredUser","185"), "Failed to connect")
        self.assertEqual(database_Login("RegisteredUser","1854"), "Connected successfully")
    
    #Agency User Login Testing
    def test_AgencyUser_database_Login(self):
        self.assertEqual(database_Login("AgencyUser","185"), "Failed to connect")
        self.assertEqual(database_Login("AgencyUser","1854"), "Connected successfully")
    
    #Administrator Login Testing
    def test_Administrator_database_Login(self):
        self.assertEqual(database_Login("Administrator","185"), "Failed to connect")
        self.assertEqual(database_Login("Administrator","1854"), "Connected successfully")

    #Non Registered user permission testing
    def test_NRU(self):
        self.assertEqual(Non_Registered_User_Permission("UPDATE NRUV SET Agency='NYP' WHERE Agency='NYPD';"), "Cannot Execute")
        self.assertEqual(Non_Registered_User_Permission("Delete from NRUV where Agency='NYPD';"), "Cannot Execute")
        self.assertEqual(Non_Registered_User_Permission("SELECT * from NRUV where Agency='NYPD' limit 2;"), "Executed")
        self.assertEqual(Agency_User_Permission("SELECT * from AgencyType limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from AgencyData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from CityData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from ComplaintsWithBorough limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from BridgeHighwayIncidentData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from TaxiIncidentData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO AgencyType(Agency) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO AgencyData(AgencyName) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO RequestData(UniqueKey) VALUES(9999999);"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO CityData(City) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO BridgeHighwayIncidentData(RoadRamp) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO TaxiIncidentData(TaxiCompanyBorough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE AgencyType SET Agency='TEST1' WHERE Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE AgencyData SET AgencyName='TEST1'where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE CityData SET City='TEST1' where City='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE ComplaintsWithBorough SET Borough='TEST1' where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE BridgeHighwayIncidentData SET RoadRamp='TEST1' where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE TaxiIncidentData SET VehicleType='TEST1' where VehicleType='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from AgencyType where Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from AgencyData where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from RequestData where UniqueKey=9999999;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from CityData where City='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from ComplaintsWithBorough where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from BridgeHighwayIncidentData where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from TaxiIncidentData where VehicleType='TEST';"), "Cannot Execute")

    #Registered user permission testing
    def test_RU(self):
        self.assertEqual(Registered_User_Permission("SELECT * from RUV limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from AgencyType limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from AgencyData limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from RequestData limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from CityData limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from ComplaintsWithBorough limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from BridgeHighwayIncidentData limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("SELECT * from TaxiIncidentData limit 2;"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO AgencyType(Agency) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO AgencyData(AgencyName) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO RequestData(UniqueKey) VALUES(9999999);"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO CityData(City) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO BridgeHighwayIncidentData(RoadRamp) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO TaxiIncidentData(TaxiCompanyBorough) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Executed")
        self.assertEqual(Registered_User_Permission("UPDATE AgencyType SET Agency='TEST1' WHERE Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE AgencyData SET AgencyName='TEST1'where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE RequestData SET UniqueKey=9999990 where UniqueKey=9999999;"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE CityData SET City='TEST1' where City='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE ComplaintsWithBorough SET Borough='TEST1' where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE BridgeHighwayIncidentData SET RoadRamp='TEST1' where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("UPDATE TaxiIncidentData SET VehicleType='TEST1' where VehicleType='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from RUV where Agency='NYPD';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from AgencyType where Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from AgencyData where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from RequestData where UniqueKey=9999999;"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from CityData where City='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from ComplaintsWithBorough where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from BridgeHighwayIncidentData where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Registered_User_Permission("Delete from TaxiIncidentData where VehicleType='TEST';"), "Cannot Execute")
    
    #Agency user permission testing
    def test_AU(self):
        self.assertEqual(Agency_User_Permission("SELECT * from AUV limit 2;"), "Executed")
        self.assertEqual(Agency_User_Permission("SELECT * from AgencyType limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from AgencyData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from RequestData limit 2;"), "Executed")
        self.assertEqual(Agency_User_Permission("SELECT * from CityData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from ComplaintsWithBorough limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from BridgeHighwayIncidentData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("SELECT * from TaxiIncidentData limit 2;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO AgencyType(Agency) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO AgencyData(AgencyName) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO RequestData(UniqueKey) VALUES(9999999);"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO CityData(City) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO BridgeHighwayIncidentData(RoadRamp) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO TaxiIncidentData(TaxiCompanyBorough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE AgencyType SET Agency='TEST1' WHERE Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE AgencyData SET AgencyName='TEST1'where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE RequestData SET UniqueKey=9999990 where UniqueKey=9999999;"), "Executed")
        self.assertEqual(Agency_User_Permission("UPDATE CityData SET City='TEST1' where City='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE ComplaintsWithBorough SET Borough='TEST1' where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE BridgeHighwayIncidentData SET RoadRamp='TEST1' where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("UPDATE TaxiIncidentData SET VehicleType='TEST1' where VehicleType='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from RUV where Agency='NYPD';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from AgencyType where Agency='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from AgencyData where AgencyName='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from RequestData where UniqueKey=9999999;"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from CityData where City='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from ComplaintsWithBorough where Borough='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from BridgeHighwayIncidentData where RoadRamp='TEST';"), "Cannot Execute")
        self.assertEqual(Agency_User_Permission("Delete from TaxiIncidentData where VehicleType='TEST';"), "Cannot Execute")

    #Registered user permission testing
    def test_Admin(self):
        self.assertEqual(Administrator_Permission("SELECT * from RUV limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from AgencyType limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from AgencyData limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from RequestData limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from CityData limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from ComplaintsWithBorough limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from BridgeHighwayIncidentData limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("SELECT * from TaxiIncidentData limit 2;"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO AgencyType(Agency) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO AgencyData(AgencyName) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO RequestData(UniqueKey) VALUES(9999999);"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO CityData(City) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO BridgeHighwayIncidentData(RoadRamp) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO TaxiIncidentData(TaxiCompanyBorough) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("INSERT IGNORE INTO ComplaintsWithBorough(Borough) VALUES('TEST');"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE AgencyType SET Agency='TEST1' WHERE Agency='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE AgencyData SET AgencyName='TEST1'where AgencyName='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE RequestData SET UniqueKey=9999990 where UniqueKey=9999999;"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE CityData SET City='TEST1' where City='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE ComplaintsWithBorough SET Borough='TEST1' where Borough='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE BridgeHighwayIncidentData SET RoadRamp='TEST1' where RoadRamp='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("UPDATE TaxiIncidentData SET VehicleType='TEST1' where VehicleType='TEST';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from AgencyType where Agency='TEST1';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from AgencyData where AgencyName='TEST1';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from RequestData where UniqueKey=9999999;"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from CityData where City='TEST1';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from ComplaintsWithBorough where Borough='TEST1';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from BridgeHighwayIncidentData where RoadRamp='TEST1';"), "Executed")
        self.assertEqual(Administrator_Permission("Delete from TaxiIncidentData where VehicleType='TEST1';"), "Executed")