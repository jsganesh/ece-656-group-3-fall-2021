#Creating a database
CREATE DATABASE Project;


# Selecting the database
USE Project;


#CREATING THE RequestData table
drop table if exists RequestData;
create table RequestData(UniqueKey int PRIMARY KEY,
						CreatedDate date check(CreatedDate>='2009-01-01'),
						ClosedDate date,
                        Agency varchar(50),
                        AgencyName varchar(100),
                        ComplaintType varchar(50),
                        Descriptor varchar(100),
                        LocationType varchar(40),
                        IncidentZip int,
                        IncidentAddress varchar(100),
                        StreetName varchar(100),
                        CrossStreet1 varchar(50),
                        CrossStreet2 varchar(50),
                        IntersectionStreet1 varchar(50),
                        IntersectionStreet2 varchar(50),
                        AddressType varchar(20),
                        City varchar(30),
                        Landmark varchar(40),
                        FacilityType VARCHAR(20),
                        Status VARCHAR(20),
                        DueDate date,
                        ResolutionDesciption varchar(1000),
                        ResolutionActionUpdateDate date,
                        CommunityBoard varchar(30),
                        BBL varchar(30),
                        Borough VARCHAR(20),
                        XCoordinate_StatePlane varchar(30),
                        YCoordinate_StatePlane varchar(30),
                        OpenDataChannelType varchar(30),
                        ParkFacilityName VARCHAR(100),
                        ParkBorough VARCHAR(20),
                        VehicleType VARCHAR(30),
                        TaxiCompanyBorough VARCHAR(20),
                        TaxiPickUpLocation VARCHAR(80),
                        BridgeHighwayName VARCHAR(50),
                        BridgeHighwayDirection VARCHAR(40),
                        RoadRamp VARCHAR(50),
                        BridgeHighwaySegment VARCHAR(120),
                        Latitude float CHECK(Latitude >= -90 AND Latitude <= 90),
                        Longitude float CHECK(Longitude >= -180 AND Longitude <= 180),
                        Location VARCHAR(50),
                        constraint CCD check((ClosedDate>=CreatedDate AND SUBSTRING(ClosedDate,1,4)<=2021) OR ClosedDate IS NULL),
                        constraint CDD check((DueDate>=CreatedDate AND SUBSTRING(DueDate,1,4)<=2021) OR DueDate IS NULL),
                        constraint CRAUD check((ResolutionActionUpdateDate>=CreatedDate AND SUBSTRING(ResolutionActionUpdateDate,1,4)<=2021) OR ResolutionActionUpdateDate IS NULL)
                        );


#Loading data into RequestData Table
load data LOCAL infile '/home/jai/Downloads/The_311_Requests.csv' ignore into table RequestData
	fields terminated by ','
 	enclosed by '"' 
	lines terminated by '\n' 
	(@UK,@CRD,@CLD,@A,@AN,@CT,@D,@LT,@IZ,@IA,@SN,@CS1,@CS2,@IS1,@IS2,@AT,@C,@L,@FT,@S,@DD,@RD,@RAUD,@CB,@BBL,@B,@XCSP,@YCSP,@ODCT,@PFN,@PB,@VT,@TCB,@TPUL,@BHN,@BHD,@RR,@BHS,@LAT,@LON,@LOC)
	 set UniqueKey = if(@UK = '', "N/A", @UK ),
		CreatedDate= STR_TO_DATE(SUBSTRING(@CRD,1,10),'%m/%d/%Y'),
		ClosedDate = STR_TO_DATE(SUBSTRING(@CLD,1,10),'%m/%d/%Y'),
		Agency = if(@A = '', "N/A", @A),
		AgencyName = if(@AN = '', "N/A", @AN),
		ComplaintType = if(@CT = '', "N/A", @CT ),
		Descriptor = if(@D = '', "N/A", @D ),
		LocationType = if(@LT = '', "N/A", @LT),
		IncidentZip = if(@IZ = '', "N/A", @IZ),
		IncidentAddress=if(@IA = '', "N/A", @IA),
		StreetName=if(@SN = '', "N/A", @SN),
		CrossStreet1=if(@CS1 = '', "N/A", @CS1),
		CrossStreet2=if(@CS2 = '', "N/A", @CS2),
		IntersectionStreet1=if(@IS1 = '', "N/A", @IS1),
		IntersectionStreet2=if(@IS2 = '', "N/A", @IS2),
		AddressType=if(@AT = '', "N/A", @AT),
		City=if(@C = '', "N/A", @C),
		Landmark=if(@L= '', "N/A", @L),
		FacilityType=if(@FT = '', "N/A", @FT),
		Status=if(@S = '', "N/A", @S),
		DueDate=STR_TO_DATE(SUBSTRING(@DD,1,10),'%m/%d/%Y'),
		ResolutionDesciption=if(@RD = '', "N/A", @RD),
		ResolutionActionUpdateDate=STR_TO_DATE(SUBSTRING(@RAUD,1,10),'%m/%d/%Y'),
		CommunityBoard=if(@CB = '', "N/A", @CB),
		BBL=if(@BBL = '', "N/A", @BBL),
		Borough=if(@B = '', "N/A", @B),
		XCoordinate_StatePlane=if(@XCSP = '', "N/A", @XCSP),
		YCoordinate_StatePlane=if(@YCSP = '', "N/A", @YCSP),
		OpenDataChannelType=if(@ODCT = '', "N/A", @ODCT),
		ParkFacilityName=if(@PFN = '', "N/A", @PFN),
		ParkBorough=if(@PB = '', "N/A", @PB),
		VehicleType=if(@VT = '', "N/A", @VT),
		TaxiCompanyBorough=if(@TCB = '', "N/A", @TCB),
		TaxiPickUpLocation=if(@TPUL = '', "N/A", @TPUL),
		BridgeHighwayName=if(@BHN = '', "N/A", @BHN),
		BridgeHighwayDirection=if(@BHD = '', "N/A", @BHD),
		RoadRamp=if(@RR = '', "N/A", @RR),
		BridgeHighwaySegment=if(@BHS = '', "N/A", @BHS),
		Latitude=if(@LAT = '', "N/A", @LAT),
		Longitude=if(@PFN = '', "N/A", @LON),
		Location=if(@LOC = '', "N/A", @LOC);


#Creating AgencyType table
drop table if exists AgencyType;
create table AgencyType(AgencyTypeID SERIAL PRIMARY KEY,
						Agency varchar(50) UNIQUE
                        );
   
   
#Inserting data into AgencyType table
INSERT INTO
	AgencyType(Agency) 
	SELECT DISTINCT
		Agency 
	from
		RequestData;


#Creating AgencyData table
drop table if exists AgencyData;
create table AgencyData(AgencyDataID SERIAL PRIMARY KEY,
						AgencyTypeID bigint unsigned,
                        Agency varchar(50),
						AgencyName varchar(100) UNIQUE
                        );
    
    
#Inserting data into AgencyData table
INSERT INTO
	AgencyData(Agency, AgencyName) 
	SELECT DISTINCT
		Agency,
		AgencyName 
	from
		RequestData 
	group by
		Agency,
		AgencyName;


#Filling AgencyTypeID values in AgencyData table using AgencyType table
update
	AgencyData 
	INNER JOIN
		AgencyType 
		ON AgencyData.Agency = AgencyType.Agency 
SET
	AgencyData.AgencyTypeID = AgencyType.AgencyTypeID 
where
	AgencyData.Agency = AgencyType.Agency;


#Adding AgencyDataID attribute to RequestData table
ALTER TABLE RequestData ADD AgencyDataID bigint unsigned;


#Filling AgencyDataID values in AgencyData table using AgencyData table
update
	RequestData 
	INNER JOIN
		AgencyData 
		ON AgencyData.Agency = RequestData.Agency 
		AND AgencyData.AgencyName = RequestData.AgencyName 
SET
	RequestData.AgencyDataID = AgencyData.AgencyDataID 
where
	AgencyData.Agency = RequestData.Agency 
	AND AgencyData.AgencyName = RequestData.AgencyName;


#Creating CityData table
drop table if exists CityData;
create table CityData(IncidentZip int PRIMARY KEY,
						City VARCHAR(30)
                        );


#Inserting data into CityData table
insert ignore into CityData(IncidentZip, City) 
SELECT DISTINCT
	IncidentZip,
	City 
FROM
	RequestData 
GROUP BY
	IncidentZip,
	City;



#Creating TaxiIncidentData table
drop table if exists TaxiIncidentData;
create table TaxiIncidentData(TaxiIncidentID SERIAL PRIMARY KEY,
							VehicleType VARCHAR(30),
							TaxiCompanyBorough VARCHAR(20),
							TaxiPickUpLocation VARCHAR(80),
							CONSTRAINT UC_Taxi UNIQUE (VehicleType,TaxiCompanyBorough,TaxiPickUpLocation)
);


#Inserting data into TaxiIncidentData table
insert ignore into TaxiIncidentData(VehicleType, TaxiCompanyBorough, TaxiPickUpLocation) 
SELECT DISTINCT
	VehicleType,
	TaxiCompanyBorough,
	TaxiPickUpLocation 
FROM
	RequestData 
GROUP BY
	VehicleType,
	TaxiCompanyBorough,
	TaxiPickUpLocation;


#Cleaning TaxiIncidentData table
delete
from
	TaxiIncidentData 
where
	VehicleType = "N/A" 
	AND TaxiCompanyBorough = "N/A" 
	AND TaxiPickUpLocation = "N/A";


#Creating BridgeHighwayIncidentData table
drop table if exists BridgeHighwayIncidentData;
create table BridgeHighwayIncidentData(BridgeHighwayIncidentID SERIAL PRIMARY KEY,
						BridgeHighwayName VARCHAR(50),
						BridgeHighwayDirection VARCHAR(40),
						RoadRamp VARCHAR(50),
						BridgeHighwaySegment VARCHAR(120),
						CONSTRAINT UC_BridgeHighway UNIQUE(BridgeHighwayName,BridgeHighwayDirection,RoadRamp,BridgeHighwaySegment)
);


#Inserting data into BridgeHighwayIncidentData table
insert ignore into BridgeHighwayIncidentData(BridgeHighwayName, BridgeHighwayDirection, RoadRamp, BridgeHighwaySegment) 
SELECT DISTINCT
	BridgeHighwayName,
	BridgeHighwayDirection,
	RoadRamp,
	BridgeHighwaySegment 
FROM
	RequestData 
GROUP BY
	BridgeHighwayName,
	BridgeHighwayDirection,
	RoadRamp,
	BridgeHighwaySegment;


#Cleaning BridgeHighwayIncidentData table
delete
from
	BridgeHighwayIncidentData 
where
	BridgeHighwayName = "N/A" 
	AND BridgeHighwayDirection = "N/A" 
	AND RoadRamp = "N/A" 
	AND BridgeHighwaySegment = "N/A";


#Creating ComplaintsWithBorough table
drop table if exists ComplaintsWithBorough;
create table ComplaintsWithBorough(BoroughID SERIAL PRIMARY KEY,
					Borough VARCHAR(20) NOT NULL,
					TaxiIncidentID bigint unsigned,
					BridgeHighwayIncidentID bigint unsigned,
					VehicleType VARCHAR(30),
					TaxiCompanyBorough VARCHAR(20),
					TaxiPickUpLocation VARCHAR(80),
					BridgeHighwayName VARCHAR(50),
					BridgeHighwayDirection VARCHAR(40),
					RoadRamp VARCHAR(50),
					BridgeHighwaySegment VARCHAR(120)
);

#inserting into ComplaintsWithBorough table    
insert ignore into ComplaintsWithBorough(Borough, VehicleType, TaxiCompanyBorough, TaxiPickUpLocation) 
SELECT DISTINCT
	Borough,
	VehicleType,
	TaxiCompanyBorough,
	TaxiPickUpLocation 
FROM
	RequestData 
where
	Borough != "N/A" 
GROUP BY
	Borough,
	VehicleType,
	TaxiCompanyBorough,
	TaxiPickUpLocation;
    
insert ignore into ComplaintsWithBorough(Borough, BridgeHighwayName, BridgeHighwayDirection, RoadRamp, BridgeHighwaySegment) 
SELECT DISTINCT
	Borough,
	BridgeHighwayName,
	BridgeHighwayDirection,
	RoadRamp,
	BridgeHighwaySegment 
FROM
	RequestData 
where
	Borough != "N/A" 
GROUP BY
	Borough,
	BridgeHighwayName,
	BridgeHighwayDirection,
	RoadRamp,
	BridgeHighwaySegment;


#Cleaning ComplaintsWithBorough table
delete
from
	ComplaintsWithBorough 
where
	Borough != "N/A"  
	AND VehicleType = "N/A" 
	AND TaxiCompanyBorough = "N/A" 
	AND TaxiPickUpLocation = "N/A" 
	AND BridgeHighwayName = "N/A" 
	AND BridgeHighwayDirection = "N/A" 
	AND RoadRamp = "N/A" 
	AND BridgeHighwaySegment = "N/A";

# Setting up NULL values as N/A in ComplaintsWithBorough table
UPDATE
	ComplaintsWithBorough 
SET
	VehicleType = "N/A" 
WHERE
	VehicleType IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	TaxiCompanyBorough = "N/A" 
WHERE
	TaxiCompanyBorough IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	TaxiPickUpLocation = "N/A" 
WHERE
	TaxiPickUpLocation IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	BridgeHighwayName = "N/A" 
WHERE
	BridgeHighwayName IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	BridgeHighwayDirection = "N/A" 
WHERE
	BridgeHighwayDirection IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	RoadRamp = "N/A" 
WHERE
	RoadRamp IS NULL;

UPDATE
	ComplaintsWithBorough 
SET
	BridgeHighwaySegment = "N/A" 
WHERE
	BridgeHighwaySegment IS NULL;


#Filling TaxiIncidentDataID values in ComplaintsWithBorough table using TaxiIncidentData table
update
	ComplaintsWithBorough 
	INNER JOIN
		TaxiIncidentData 
		ON ComplaintsWithBorough.VehicleType = TaxiIncidentData.VehicleType 
		AND ComplaintsWithBorough.TaxiCompanyBorough = TaxiIncidentData.TaxiCompanyBorough 
		AND ComplaintsWithBorough.TaxiPickUpLocation = TaxiIncidentData.TaxiPickUpLocation 
SET
	ComplaintsWithBorough.TaxiIncidentID = TaxiIncidentData.TaxiIncidentID 
where
	ComplaintsWithBorough.VehicleType = TaxiIncidentData.VehicleType 
	AND ComplaintsWithBorough.TaxiCompanyBorough = TaxiIncidentData.TaxiCompanyBorough 
	AND ComplaintsWithBorough.TaxiPickUpLocation = TaxiIncidentData.TaxiPickUpLocation;
    

#Filling BridgeHighwayIncidentDataID values in ComplaintsWithBorough table using BridgeHighwayIncidentData table
update
	ComplaintsWithBorough 
	INNER JOIN
		BridgeHighwayIncidentData 
		ON ComplaintsWithBorough.BridgeHighwayName = BridgeHighwayIncidentData.BridgeHighwayName 
		AND ComplaintsWithBorough.BridgeHighwayDirection = BridgeHighwayIncidentData.BridgeHighwayDirection 
		AND ComplaintsWithBorough.RoadRamp = BridgeHighwayIncidentData.RoadRamp 
		AND ComplaintsWithBorough.BridgeHighwaySegment = BridgeHighwayIncidentData.BridgeHighwaySegment 
SET
	ComplaintsWithBorough.BridgeHighwayIncidentID = BridgeHighwayIncidentData.BridgeHighwayIncidentID 
where
	ComplaintsWithBorough.BridgeHighwayName = BridgeHighwayIncidentData.BridgeHighwayName 
	AND ComplaintsWithBorough.BridgeHighwayDirection = BridgeHighwayIncidentData.BridgeHighwayDirection 
	AND ComplaintsWithBorough.RoadRamp = BridgeHighwayIncidentData.RoadRamp 
	AND ComplaintsWithBorough.BridgeHighwaySegment = BridgeHighwayIncidentData.BridgeHighwaySegment;


#Adding BoroughID attribute to RequestData table
ALTER TABLE RequestData ADD BoroughID bigint unsigned;


#Creating an index on RequestData for faster referencing while filling BoroughID
create index idcreqdat 
on RequestData(Borough, VehicleType, TaxiCompanyBorough, TaxiPickUpLocation, BridgeHighwayName, BridgeHighwayDirection, RoadRamp, BridgeHighwaySegment);


#Filling BoroughID values in AgencyData table using ComplaintsWithBorough table
update
	RequestData 
	INNER JOIN
		ComplaintsWithBorough 
		ON RequestData.Borough = ComplaintsWithBorough.Borough 
		AND RequestData.VehicleType = ComplaintsWithBorough.VehicleType 
		AND RequestData.TaxiCompanyBorough = ComplaintsWithBorough.TaxiCompanyBorough 
		AND RequestData.TaxiPickUpLocation = ComplaintsWithBorough.TaxiPickUpLocation 
		AND RequestData.BridgeHighwayName = ComplaintsWithBorough.BridgeHighwayName 
		AND RequestData.BridgeHighwayDirection = ComplaintsWithBorough.BridgeHighwayDirection 
		AND RequestData.RoadRamp = ComplaintsWithBorough.RoadRamp 
		AND RequestData.BridgeHighwaySegment = ComplaintsWithBorough.BridgeHighwaySegment 
SET
	RequestData.BoroughID = ComplaintsWithBorough.BoroughID;


#Deleting the index after work
drop 
  index idcreqdat on RequestData;


#Cleaning the unwanted attributes from AgencyData table
Alter table 
  AgencyData 
drop 
  column Agency;


#Cleaning the unwanted attributes from RequestData table
Alter table 
  RequestData 
drop 
  column Agency;

Alter table 
  RequestData 
drop 
  column AgencyName;

Alter table 
  RequestData 
drop 
  column Borough;

Alter table 
  RequestData 
drop 
  column City;

Alter table 
  RequestData 
drop 
  column VehicleType;

Alter table 
  RequestData 
drop 
  column TaxiCompanyBorough;

Alter table 
  RequestData 
drop 
  column TaxiPickUpLocation;

Alter table 
  RequestData 
drop 
  column BridgeHighwayName;

Alter table 
  RequestData 
drop 
  column BridgeHighwayDirection;

Alter table 
  RequestData 
drop 
  column RoadRamp;

Alter table 
  RequestData 
drop 
  column BridgeHighwaySegment;


#Cleaning the unwanted attributes from ComplaintsWithBorough table
Alter table 
  ComplaintsWithBorough 
drop 
  column VehicleType;

Alter table 
  ComplaintsWithBorough 
drop 
  column TaxiCompanyBorough;

Alter table 
  ComplaintsWithBorough 
drop 
  column TaxiPickUpLocation;

Alter table 
  ComplaintsWithBorough 
drop 
  column BridgeHighwayName;

Alter table 
  ComplaintsWithBorough 
drop 
  column BridgeHighwayDirection;

Alter table 
  ComplaintsWithBorough 
drop 
  column RoadRamp;

Alter table 
  ComplaintsWithBorough 
drop 
  column BridgeHighwaySegment;


#Adding Foreign Key Constraints between different tables
ALTER TABLE 
  AgencyData 
ADD 
  CONSTRAINT FK_AgencyData_To_AgencyType FOREIGN KEY (AgencyTypeID) REFERENCES AgencyType(AgencyTypeID);

ALTER TABLE 
  RequestData 
ADD 
  CONSTRAINT FK_RequestData_To_AgencyData FOREIGN KEY (AgencyDataID) REFERENCES AgencyData(AgencyDataID);

ALTER TABLE 
  RequestData 
ADD 
  CONSTRAINT FK_RequestData_To_ComplaintsWithBorough FOREIGN KEY (BoroughID) REFERENCES ComplaintsWithBorough(BoroughID);

ALTER TABLE 
  RequestData 
ADD 
  CONSTRAINT FK_RequestData_To_CityData FOREIGN KEY (IncidentZip) REFERENCES CityData(IncidentZip);

ALTER TABLE 
  ComplaintsWithBorough 
ADD 
  CONSTRAINT FK_ComplaintsWithBorough_To_TaxiIncidentData FOREIGN KEY (TaxiIncidentID) REFERENCES TaxiIncidentData(TaxiIncidentID);

ALTER TABLE 
  ComplaintsWithBorough 
ADD 
  CONSTRAINT FK_ComplaintsWithBorough_To_BridgeHighwayIncidentData FOREIGN KEY (BridgeHighwayIncidentID) REFERENCES BridgeHighwayIncidentData(BridgeHighwayIncidentID);
  

#Adding Indexes to make queries run faster
CREATE INDEX ACY on RequestData(
  AgencyDataID, CreatedDate, IncidentZip
);
CREATE INDEX ND on BridgeHighwayIncidentData(
  BridgeHighwayName, BridgeHighwayDirection
);
CREATE INDEX CYB on RequestData(
  IncidentZip, CreatedDate, BoroughID
);


#Adding different users
CREATE USER 'NonRegisteredUser'@'localhost' IDENTIFIED BY '1854';
CREATE USER 'RegisteredUser'@'localhost' IDENTIFIED by '1854';
CREATE USER 'Administrator'@'localhost' IDENTIFIED by '1854';
CREATE USER 'AgencyUser'@'localhost' IDENTIFIED by '1854';


#Creating Views
CREATE VIEW NRUV AS 
SELECT 
  A.Agency as Agency, 
  SUBSTRING(R.CreatedDate, 1, 4) as CreatedYear, 
  C.City as City, 
  CB.taxiIncidentID as TaxiIncidentID, 
  CB.BridgeHighwayIncidentID as BridgeHighwayIncidentID 
FROM 
  AgencyType A, 
  AgencyData AD, 
  RequestData R, 
  CityData C, 
  ComplaintsWithBorough CB 
WHERE 
  A.AgencyTypeID = AD.AgencyTypeID 
  AND AD.AgencyDataID = R.AgencyDataID 
  AND R.IncidentZip = C.IncidentZip 
  AND CB.BoroughID = R.BoroughID;

CREATE VIEW RUV AS 
SELECT 
  A.Agency as Agency, 
  SUBSTRING(R.CreatedDate, 1, 4) as CreatedYear, 
  R.ClosedDate, 
  C.City as City, 
  CB.taxiIncidentID as TaxiIncidentID, 
  CB.BridgeHighwayIncidentID as BridgeHighwayIncidentID, 
  TID.VehicleType as VehicleType 
FROM 
  AgencyType A, 
  AgencyData AD, 
  RequestData R, 
  CityData C, 
  ComplaintsWithBorough CB, 
  TaxiIncidentData TID 
WHERE 
  A.AgencyTypeID = AD.AgencyTypeID 
  AND AD.AgencyDataID = R.AgencyDataID 
  AND R.IncidentZip = C.IncidentZip 
  AND CB.BoroughID = R.BoroughID 
  AND CB.TaxiIncidentID = TID.TaxiIncidentID;

CREATE VIEW AUV AS 
SELECT 
  UniqueKey, 
  Agency, 
  CreatedDate, 
  City, 
  ComplaintType, 
  Descriptor, 
  Status, 
  IncidentAddress, 
  Landmark, 
  ResolutionActionUpdateDate, 
  ResolutionDesciption, 
  ClosedDate 
FROM 
  AgencyType A, 
  AgencyData AD, 
  RequestData R, 
  CityData C 
WHERE 
  A.AgencyTypeID = AD.AgencyTypeID 
  AND AD.AgencyDataID = R.AgencyDataID 
  AND R.IncidentZip = C.IncidentZip;


#Granting Permissions to different users
GRANT SELECT ON Project.NRUV to 'NonRegisteredUser'@'localhost';
GRANT SELECT,INSERT ON Project.*  TO 'RegisteredUser'@'localhost';
GRANT SELECT ON Project.RUV to 'RegisteredUser'@'localhost';
GRANT SELECT,UPDATE ON Project.AUV to 'AgencyUser'@'localhost';
GRANT UPDATE ON Project.RequestData to 'AgencyUser'@'localhost';
GRANT ALL ON Project.* to 'Administrator'@'localhost'; 